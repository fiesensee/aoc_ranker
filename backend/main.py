from flask import Flask
from flask_caching import Cache
from flask_cors import CORS
import requests

app = Flask(__name__)
CORS(app)
cache = Cache(app, config={"CACHE_TYPE": "simple"})

@app.route("/ranks")
@cache.cached(timeout=900)
def get_ranks():
    cookies = {'session': '53616c7465645f5ffbc4cc8ee449b4c0f88035ce0d9b82255d29729d1b7d6591d74584d105c5e34b1cbed67021bb0c86'}
    response = requests.get("https://adventofcode.com/2019/leaderboard/private/view/398495.json", cookies=cookies)
    return response.json()