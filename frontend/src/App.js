import React, { useState, useEffect } from 'react';
import axios from 'axios';
import { Container } from '@material-ui/core'
import './App.css';
import RankComponent from './RankComponent'

function App() {
  const [data, setData] = useState(undefined);
  useEffect(() => {
    async function fetchData() {
      const result = await axios(
        'https://aoc.isensee.dev/ranks',
      );
      // console.debug(result.data.members)
      setData(result.data.members);
    }
    fetchData();
  }, []);

  return (
    <Container>
      <RankComponent data={data} />
    </Container>
  );
}

export default App;
