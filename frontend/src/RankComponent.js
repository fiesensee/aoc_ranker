import React from 'react';
import CircularProgress from '@material-ui/core/CircularProgress'
import { Paper, Grid } from '@material-ui/core';
import moment from 'moment'
import { makeStyles } from '@material-ui/core/styles';
import { LineChart, Line, Legend, XAxis, YAxis, Tooltip } from 'recharts'
import randomColor from 'randomcolor'

const useStyles = makeStyles(theme => ({
    root: {
        padding: theme.spacing(5),
        paddingTop: theme.spacing(1),
    },
}));

function RankComponent(props) {
    const classes = useStyles();

    if (props.data === undefined) {
        return (
            <CircularProgress />
        );
    }

    // ugly data reshaping

    const memberData = props.data
    const days = []
    for (let member_number in memberData) {
        let member = memberData[member_number]
        for (let dayNumber in member.completion_day_level) {
            let index = dayNumber - 1
            if (days[index] === undefined) {
                days.push([])
            }
            let dayData = days[index]
            let day = member.completion_day_level[dayNumber]
            let memberObject = {
                "name": member.name,
                "first": moment(day[1].get_star_ts * 1000),
                "dayScore": 0,
                "totalScore": 0,
            }
            if (day[2] !== undefined) {
                memberObject.second = moment(day[2].get_star_ts * 1000)
            }
            dayData.push(memberObject)
        }
    }

    const totals = Object.entries(calculateScores(days))

    totals.sort((x, y) => y[1] - x[1])

    const graphData = getGraphData(days)

    return (
        <Grid
            container
            direction="column"
            spacing={3}
            justify="center"
        >
            <Grid item>
                <Paper className={classes.root}>
                    <h3>Totals over time</h3>
                    <LineChart width={1030} height={450} data={graphData}
                        margin={{ top: 5, right: 30, left: 20, bottom: 5 }}>
                        <Legend />
                        <XAxis dataKey="name" />
                        <YAxis />
                        <Tooltip />
                        {totals.map((total) => 
                            <Line type="monotone" connectNulls dataKey={total[0]} key={total[0]} stroke={randomColor({luminosity: 'dark'})}/>
                        )}
                    </LineChart>
                </Paper>
            </Grid>
            {days.reverse().map((day, index) =>
                <Grid item key={index}>
                    <Paper className={classes.root}>
                        <h3>{days.length - index}.12.</h3>
                        <Grid
                            container
                            spacing={5}
                            justify="space-between"
                        >
                            {day.map((member) =>
                                renderMemberData(member)
                            )}
                        </Grid>
                    </Paper>
                </Grid>
            )}
        </Grid>
    );
}

function getGraphData(days) {
    const data = []
    days.forEach((day, index) => {
        const dayData = { name: (index + 1) + ".12." }
        day.forEach((member) => {
            dayData[member.name] = member.totalScore
        })
        data.push(dayData)
    })

    return data
}

function calculateScores(days) {
    const totals = {}
    for (let index in days) {
        let day = days[index]

        // first star calc
        day = day.sort((x, y) => {
            return x.first - y.first
        })
        day.forEach((member, index) => {
            member.dayScore += (7 - index)
        })

        // second star calc
        day = day.sort((x, y) => {
            return x.second - y.second
        })
        day.forEach((member, index) => {
            if (member.second !== undefined) {
                member.dayScore += (7 - index)
            }
        })

        // total star calc
        day.forEach((member) => {
            if (totals[member.name] === undefined) {
                totals[member.name] = 0
            }
            totals[member.name] += member.dayScore
            member.totalScore = totals[member.name]
        })

        // sort by day score (show "day winner")
        day = day.sort((x, y) => {
            return  y.dayScore - x.dayScore
        })
    }

    return totals
}

function renderMemberData(member) {
    return (
        <div key={member.name}>
            <h4>{member.name} (Total: {member.totalScore})</h4>
            <h5>Got {member.dayScore} Points</h5>
            {renderTimeData(member)}
        </div>
    );
}

function renderTimeData(times) {
    let secondTimeData = ""
    if (times.second !== undefined) {
        secondTimeData =
            <div>
                <p>2. Star: {times.second.format("D MMM  HH:mm:ss")}</p>
                <p>Diff: {moment.duration(times.first.diff(times.second)).humanize()}</p>
            </div>
    }
    return (
        <div>
            <p>1. Star: {times.first.format("D MMM  HH:mm:ss")}</p>
            {secondTimeData}
        </div>

    )
}

export default RankComponent;